-- Insert roles
insert into role (name) values ('ROLE_USER')

-- Insert users
insert into user(username, enabled, password, role_id) values ('user', true, '12345', 1)
insert into user(username, enabled, password, role_id) values ('user2', true, '12345', 1)

-- Insert customers
insert into customer (totalAmount, totalVat) values (0.00, 0.00)
insert into customer (totalAmount, totalVat) values (0.00, 0.00)
insert into customer (totalAmount, totalVat) values (0.00, 0.00)
insert into customer (id, totalAmount, totalVat) values (1234, 110.00, 11.00)

-- Insert customers
insert into invoice (invoice_id, amount, vat, customer_id) values (1, 100.00, 10.00, 1)
insert into invoice (invoice_id, amount, vat, customer_id) values (2, 200.00, 20.00, 2)
insert into invoice (invoice_id, amount, vat, customer_id) values (3, 200.00, 20.00, 2)
insert into invoice (invoice_id, amount, vat, customer_id) values (4, 200.00, 20.00, 3)
insert into invoice (invoice_id, amount, vat, customer_id) values (5, 200.00, 20.00, 3)
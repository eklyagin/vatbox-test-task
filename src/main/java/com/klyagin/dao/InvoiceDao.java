package com.klyagin.dao;

import com.klyagin.model.Invoice;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface InvoiceDao extends CrudRepository<Invoice, Long> {
    List<Invoice> findAll();
}

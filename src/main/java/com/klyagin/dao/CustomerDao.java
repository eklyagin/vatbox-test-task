package com.klyagin.dao;

import com.klyagin.model.Customer;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CustomerDao extends CrudRepository<Customer, Long> {
    List<Customer> findAll();
}

package com.klyagin.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;

@Entity
public class Customer implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private BigDecimal totalAmount;
    private BigDecimal totalVat;
    @OneToMany(mappedBy = "customer", cascade = CascadeType.ALL)
    @Fetch(FetchMode.SUBSELECT)
    @JsonIgnore
    private Set<Invoice> invoices;

    public Customer(){
        invoices = new HashSet<>();
    }

    public Customer(BigDecimal totalAmount, BigDecimal totalVat) {
        this();
        this.totalAmount = totalAmount;
        this.totalVat = totalVat;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Set<Invoice> getInvoices() {
        return invoices;
    }

    public void addInvoice(Invoice invoice){
        invoice.setCustomer(this);
        invoices.add(invoice);
    }

    public BigDecimal getTotalAmount() {
        return totalAmount.add(invoices.stream()
                .map(Invoice::getAmount)
                .reduce(BigDecimal.ZERO, BigDecimal::add));
    }

    public BigDecimal getTotalVat() {
        return totalVat.add(invoices.stream()
                .map(Invoice::getVat)
                .reduce(BigDecimal.ZERO, BigDecimal::add));
    }

    public void setTotalAmount(BigDecimal totalAmount) {
        this.totalAmount = totalAmount;
    }

    public void setTotalVat(BigDecimal totalVat) {
        this.totalVat = totalVat;
    }

}

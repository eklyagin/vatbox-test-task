package com.klyagin.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;

@Entity
public class Invoice implements Serializable {
    @Id
    private Long invoice_id;
    private BigDecimal amount;
    private BigDecimal vat;
    @Transient
    private Long customer_id;
    @ManyToOne
    @JsonIgnore
    private Customer customer;


    protected Invoice(){
    }

    public Invoice(BigDecimal amount, BigDecimal vat) {
        this();
        this.amount = amount;
        this.vat = vat;
    }

    public Invoice(Long invoice_id, BigDecimal amount, BigDecimal vat, Long customer_id) {
        this.invoice_id = invoice_id;
        this.amount = amount;
        this.vat = vat;
        this.customer_id = customer_id;
    }

    public Long getInvoice_id() {
        return invoice_id;
    }

    public void setInvoice_id(Long invoice_id) {
        this.invoice_id = invoice_id;
    }

    public Long getCustomer_id() {
        return customer_id;
    }

    public void setCustomer_id(Long customer_id) {
        this.customer_id = customer_id;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public BigDecimal getVat() {
        return vat;
    }

    public void setVat(BigDecimal vat) {
        this.vat = vat;
    }
}

package com.klyagin.web;

import com.klyagin.model.Customer;
import com.klyagin.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class CustomerController {
    @Autowired
    private CustomerService customerService;

    @RequestMapping("/customers")
    public List<Customer> customers(){
        return customerService.findAllCustomers();
    }

    @RequestMapping("/customers/{id}")
    public Customer getCustomerById(@PathVariable Long id){
        Customer customer = customerService.findCustomerById(id);
        return customer;
    }

}

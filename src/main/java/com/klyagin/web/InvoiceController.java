package com.klyagin.web;

import com.klyagin.model.Customer;
import com.klyagin.model.Invoice;
import com.klyagin.service.CustomerService;
import com.klyagin.service.InvoiceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.view.RedirectView;

import java.util.List;

@RestController
public class InvoiceController {
    @Autowired
    private InvoiceService invoiceService;
    @Autowired
    private CustomerService customerService;

    @RequestMapping("/invoices")
    public List<Invoice> invoices(){
        return invoiceService.findAllInvoices();
    }

    @RequestMapping(path = "/invoices", method = RequestMethod.POST)
    public RedirectView addInvoice(@RequestBody Invoice invoice){
        Customer customer = customerService.findCustomerById(invoice.getCustomer_id());
        invoiceService.save(invoice, customer);
        return new RedirectView("/customers/" + invoice.getCustomer_id());
    }
}

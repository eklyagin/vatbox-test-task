package com.klyagin.service;

import com.klyagin.model.Customer;
import com.klyagin.model.Invoice;

import java.util.List;

public interface InvoiceService {
    List<Invoice> findAllInvoices();
    void save(Invoice invoice, Customer customer);
}

package com.klyagin.service;

import com.klyagin.dao.CustomerDao;
import com.klyagin.model.Customer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CustomerServiceImpl implements CustomerService {
    @Autowired
    private CustomerDao customerDao;
    @Override
    public List<Customer> findAllCustomers() {
        return customerDao.findAll();
    }

    @Override
    public Customer findCustomerById(Long id) {
        return customerDao.findOne(id);
    }

    @Override
    public void save(Customer customer) {
        customerDao.save(customer);
    }
}

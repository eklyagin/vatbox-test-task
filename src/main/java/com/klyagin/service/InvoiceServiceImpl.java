package com.klyagin.service;

import com.klyagin.dao.InvoiceDao;
import com.klyagin.model.Customer;
import com.klyagin.model.Invoice;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class InvoiceServiceImpl implements InvoiceService{
    @Autowired
    private InvoiceDao invoiceDao;


    @Override
    public List<Invoice> findAllInvoices() {
        return invoiceDao.findAll();
    }

    @Override
    public void save(Invoice invoice, Customer customer) {
        invoice.setCustomer(customer);
        invoiceDao.save(invoice);
    }
}

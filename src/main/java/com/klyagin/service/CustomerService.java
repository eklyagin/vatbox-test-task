package com.klyagin.service;

import com.klyagin.model.Customer;

import java.util.List;

public interface CustomerService {
    List<Customer> findAllCustomers();
    Customer findCustomerById(Long id);
    void save(Customer customer);
}
